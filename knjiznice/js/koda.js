
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
 var JanezEHR = "";
 var JozeEHR = "";
 var MirkoEHR = "";
 
function generirajPodatke(stPacienta) {
  var sessionId = getSessionId();
    $.ajaxSetup({
	    headers: {"Ehr-Session": sessionId}
	});
    
    if (stPacienta == 0 || JanezEHR == ""){
    $.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: "Janez",
		            lastNames: "Novak",
		            gender: "Moski",
		            city: "Jesenice",
		            dateOfBirth: "1971-03-18",
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		    
		    JanezEHR=ehrId;
		        
		    $.ajax({
		        url: baseUrl + "/demographics/party",
		        type: 'POST',
		        contentType: 'application/json',
		        data: JSON.stringify(partyData),
	        });
	        
	        //dodamo še nekaj podatkov o njihovem zdravju
	   
        	var telesnaVisina = 189
        	var telesnaTeza = 99
        	var telesnaTemperatura = 36
        	var sistolicniKrvniTlak = 81
        	var diastolicniKrvniTlak = 145
        	var nasicenostKrviSKisikom = 99
        	
        	var podatki={
        	    "ctx/language": "en",
    		    "ctx/territory": "SI",
    		    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
    		    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
    		   	"vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
    		    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
    		    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
    		    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
    		    "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
        	};
	       
	       JanezProbs =[
        	    "Poje preveč mesa."
        	   ];
	       
	       
	       var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		   };
	       
	      $.ajax({
		    url: baseUrl + "/composition?" + $.param(parametriZahteve),
		    type: 'POST',
		    contentType: 'application/json',
		    data: JSON.stringify(podatki),
		})
	       
	    }
	});
	
	
		
    $.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: "Joze",
		            lastNames: "Kovac",
		            gender: "MALE",
		            dateOfBirth: "1980-10-06",
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        
		        JozeEHR=ehrId;
		        
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		        });
		        
	   
        	var telesnaVisina = 182
        	var telesnaTeza = 83
        	var telesnaTemperatura = 38
        	var sistolicniKrvniTlak = 70
        	var diastolicniKrvniTlak = 115
        	var nasicenostKrviSKisikom = 90
        	
        	var podatki={
        	    "ctx/language": "en",
    		    "ctx/territory": "SI",
    		    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
    		    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
    		   	"vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
    		    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
    		    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
    		    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
    		    "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
        	};
	       
	       JozeProbs =[
        	    "Smokes too much",
        	    "Drinks too much",
        	    "Low blood preasure"
        	   ];
        	   

	       var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		   };
	       
	        $.ajax({
		        url: baseUrl + "/composition?" + $.param(parametriZahteve),
		        type: 'POST',
		        contentType: 'application/json',
		        data: JSON.stringify(podatki),
		    })

		    }
		});
	
	
		
    $.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: "Mirko",
		            lastNames: "Visavski",
		            gender: "MALE",
		            dateOfBirth: "2000-04-05",
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        
		        MirkoEHR=ehrId;
		        
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		        });
		        
		        		        //dodamo še nekaj podatkov o njihovem zdravju
	   
        	var telesnaVisina = 165
        	var telesnaTeza = 56
        	var telesnaTemperatura = 40
        	var sistolicniKrvniTlak = 90
        	var diastolicniKrvniTlak = 140
        	var nasicenostKrviSKisikom = 94
        	
        	var podatki={
        	    "ctx/language": "en",
    		    "ctx/territory": "SI",
    		    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
    		    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
    		   	"vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
    		    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
    		    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
    		    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
    		    "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
        	};
        	
        	MirkoProbs =[
        	    "Fractured tibia",
        	    "High fever",
        	    "High blood preasure"
        	   ];
        
	       
	       var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		   };
	       
	        $.ajax({
		        url: baseUrl + "/composition?" + $.param(parametriZahteve),
		        type: 'POST',
		        contentType: 'application/json',
		        data: JSON.stringify(podatki),
		    })
		        
		    }
		});
    }
		if (stPacienta == 3) getJanez();
		if (stPacienta == 2) getJoze();
		if (stPacienta == 1) getMirko();
  return 0;
}

function getJanez(){
    sessionId = getSessionId();
    
    $.ajaxSetup({
	    headers: {"Ehr-Session": sessionId}
	});
	
	if (JanezEHR != ""){
    $('#predstavitev').html("");
    $('#predstavitev').append("\
        <img id='profilPic' src='https://upload.wikimedia.org/wikipedia/commons/thumb/2/2e/Stephen_Weiss.jpg/220px-Stephen_Weiss.jpg' class='img-rounded' style='width:100px; float:left; margin-top:20px;'>\
            <div style='float:left; margin-left: 10px;'>\
                <h3><span id='ime'> Janez </span> <span id='priimek'> Novak</span></h3>\
                <p><b>Spol:</b><span id='spol'> moški</span></p>\
                <p><b>Datum rojstva:</b><span id='DOB'> 18.3.1971 </span></p>\
                <p><b>Mesto:</b><span id='city'> Jesenice </span></p>\
                <p><b>Dejavnost:</b><span> Hokej</span></p>\
                <p><b>EhrID: </b><span>"+ JanezEHR +"</span></p>\
            </div>\  ");}
            else{
            	$('#predstavitev').html("");
    $('#predstavitev').append("\
        <img id='profilPic' src='https://upload.wikimedia.org/wikipedia/commons/thumb/2/2e/Stephen_Weiss.jpg/220px-Stephen_Weiss.jpg' class='img-rounded' style='width:100px; float:left; margin-top:20px;'>\
            <div style='float:left; margin-left: 10px;'>\
                <h3><span id='ime'> Janez </span> <span id='priimek'> Novak</span></h3>\
                <p><b>Spol:</b><span id='spol'> moški</span></p>\
                <p><b>Datum rojstva:</b><span id='DOB'> 18.3.1971 </span></p>\
                <p><b>Mesto:</b><span id='city'> Jesenice </span></p>\
                <p><b>Dejavnost:</b><span> Hokej</span></p>\
                <p><b>EhrID: </b><span>NAJPREJ ZGENERIRANJTE PODATKE!</span></p>\
            </div>\  ");}
}


function getJoze(){
    sessionId = getSessionId();
    
    $.ajaxSetup({
	    headers: {"Ehr-Session": sessionId}
	});
	
	if (JozeEHR != ""){
    $('#predstavitev').html("");
    $('#predstavitev').append("\
        <img id='profilPic' src='http://iscreamsundae.com/wp-content/uploads/2015/12/farmers.jpg' class='img-rounded' style='width:100px; float:left; margin-top:20px;'>\
            <div style='float:left; margin-left: 10px;'>\
                <h3><span id='ime'> Jože </span> <span id='priimek'> Kovač</span></h3>\
                <p><b>Spol:</b><span id='spol'> moški</span></p>\
                <p><b>Datum rojstva:</b><span id='DOB'> 17.4.1980 </span></p>\
                <p><b>Mesto:</b><span id='city'> Kočevje </span></p>\
                <p><b>Dejavnost:</b><span> Kmetovanje</span></p>\
                <p><b>EhrID: </b><span>"+ JozeEHR +"</span></p>\
            </div>\  ");
		}else{
			$('#predstavitev').html("");
    $('#predstavitev').append("\
        <img id='profilPic' src='http://iscreamsundae.com/wp-content/uploads/2015/12/farmers.jpg' class='img-rounded' style='width:100px; float:left; margin-top:20px;'>\
            <div style='float:left; margin-left: 10px;'>\
                <h3><span id='ime'> Jože </span> <span id='priimek'> Kovač</span></h3>\
                <p><b>Spol:</b><span id='spol'> moški</span></p>\
                <p><b>Datum rojstva:</b><span id='DOB'> 17.4.1980 </span></p>\
                <p><b>Mesto:</b><span id='city'> Kočevje </span></p>\
                <p><b>Dejavnost:</b><span> Kmetovanje</span></p>\
                <p><b>EhrID: </b><span>NAJPREJ ZGENERIRANJTE PODATKE!</span></p>\
            </div>\  ");
		}
            
}

function getMirko(){
    sessionId = getSessionId();
    
    $.ajaxSetup({
	    headers: {"Ehr-Session": sessionId}
	});
	
	if (MirkoEHR != ""){
    $('#predstavitev').html("");
    $('#predstavitev').append("\
        <img id='profilPic' src='https://yt3.ggpht.com/-A5r_mz9oqRU/AAAAAAAAAAI/AAAAAAAAAAA/xskJtxojP4c/s900-c-k-no-mo-rj-c0xffffff/photo.jpg' class='img-rounded' style='width:100px; float:left; margin-top:20px;'>\
            <div style='float:left; margin-left: 10px;'>\
                <h3><span id='ime'> Mirko </span> <span id='priimek'> Višavski</span></h3>\
                <p><b>Spol:</b><span id='spol'> moški</span></p>\
                <p><b>Datum rojstva:</b><span id='DOB'> 4.5.2000 </span></p>\
                <p><b>Mesto:</b><span id='city'> Ljubljana </span></p>\
                <p><b>Dejavnost:</b><span> Igračkar</span></p>\
                <p><b>EhrID: </b><span>"+ MirkoEHR +"</span></p>\
            </div>\  ");}

            else{
            	$('#predstavitev').html("");
    			$('#predstavitev').append("\
        <img id='profilPic' src='https://yt3.ggpht.com/-A5r_mz9oqRU/AAAAAAAAAAI/AAAAAAAAAAA/xskJtxojP4c/s900-c-k-no-mo-rj-c0xffffff/photo.jpg' class='img-rounded' style='width:100px; float:left; margin-top:20px;'>\
            <div style='float:left; margin-left: 10px;'>\
                <h3><span id='ime'> Mirko </span> <span id='priimek'> Višavski</span></h3>\
                <p><b>Spol:</b><span id='spol'> moški</span></p>\
                <p><b>Datum rojstva:</b><span id='DOB'> 4.5.2000 </span></p>\
                <p><b>Mesto:</b><span id='city'> Ljubljana </span></p>\
                <p><b>Dejavnost:</b><span> Igračkar</span></p>\
                <p><b>EhrID: </b><span>NAJPREJ ZGENERIRANJTE PODATKE!</span></p>\
            </div>\  ");}
            
            
}


function pridobiEHR(){
	var EHR=$("#ehrVsebina").val();
	var sessionId = getSessionId();
    
    $.ajaxSetup({
	    headers: {"Ehr-Session": sessionId}
	});
	
	var ime="Prišlo je do napake, poiskusite ponovno."
	var priimek=""
	var spol="?"
	var datumRojstva="?"
	var mesto="?"
	
	
	$.ajax({
		url:baseUrl+"/demographics/ehr/"+EHR+"/party",
		type: 'GET',
		success: function(data){
			var party=data.party;
			ime=party.firstNames;
			priimek=party.lastNames;
			spol=party.gender;
			datumRojstva=party.dateOfBirth;
			mesto=party.city;
			
			
			
			$('#predstavitev').html("");
		    $('#predstavitev').append("\
		        <img id='profilPic' src='http://www.sioug.si/images/SIOUG/2016_SIOUG/Java_si_predavatelji/unknown-user.png' class='img-rounded' style='width:100px; float:left; margin-top:20px;'>\
	            <div style='float:left; margin-left: 10px;'>\
	                <h3><span id='ime'> "+ime+" </span> <span id='priimek'> "+priimek+"</span></h3>\
	                <p><b>Spol:</b><span id='spol'> "+spol+"</span></p>\
	                <p><b>Datum rojstva:</b><span id='DOB'> "+ datumRojstva +" </span></p>\
	                <p><b>Mesto:</b><span id='city'> "+mesto+"</span></p>\
	                <p><b>EhrID: </b><span> "+ EHR +" </span></p>\
	            </div>\  ");
		}
	});
};

$(document).ready(function(){
	generirajPodatke(stPacienta);
	})

